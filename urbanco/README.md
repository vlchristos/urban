# Urban – SPA Booking system assignment

##### You can see a live demo [here](https://urban.vlchristos.com).

--

---

### CORS, unfortunately

If you have problems with CORS and no data is displayed, you can either use a browser extension like [this one](https://mybrowseraddon.com/access-control-allow-origin.html) to overcome it or follow the instructions on the dialog when clicking the 'CORS' button in the app (bottom left corner). Or please fix it once and for all on the the server side.

---

### Run localy on development

Download the project and navigate to `/urbanco` folder.

##### Install packages with

`yarn`
or
`npm install`

##### Run the app by

`yarn start`
or
`npm start`
