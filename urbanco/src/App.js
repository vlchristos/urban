import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { persistor, store } from './store/store';
import { PersistGate } from 'redux-persist/integration/react';
import { ThemeProvider } from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';
// import PageStructure from './pages/PageStructure';
import { themeOptions } from './themeOptions';
import PagesWrapper from './pages/PagesWrapper';

function App() {
    const theme = themeOptions();
    return (
        <BrowserRouter>
            <Provider store={store}>
                <PersistGate persistor={persistor} onBeforeLift={null}>
                    <ThemeProvider theme={theme}>
                        <CssBaseline />
                        <div className='App'>
                            <PagesWrapper />
                        </div>
                    </ThemeProvider>
                </PersistGate>
            </Provider>
        </BrowserRouter>
    );
}

export default App;
