import React from 'react';
import { Divider, ListItem, ListItemText, Typography } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import AvailableSlotsPriceIcon from './AvailableSlotsPriceIcon';
import AvailableSlotsWorkersCount from './AvailableSlotsWorkersCount';
import { setSelectedSlot } from '../../store/global/globalActions';
import { useHistory } from 'react-router';
import AvailableSlotsListSkeleton from './AvailableSlotsListSkeleton';
import { isEmpty } from 'lodash';

const AvailableSlotsListItem = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const slotsList = useSelector((state) => state.availableSlots.slotsList);
    const basketItems = useSelector((state) => state.global.basketItems);

    function selectTimeSlot(slot) {
        dispatch(setSelectedSlot(slot));
        history.push('/workers');
    }

    function alreadyIn(id) {
        return basketItems.some((item) => item.slot_id === id);
    }

    return (
        <>
            {isEmpty(slotsList) ? (
                <AvailableSlotsListSkeleton />
            ) : (
                <>
                    {slotsList.map((slot) => (
                        <React.Fragment key={slot.id}>
                            <ListItem
                                button
                                disabled={slot.unavailable}
                                onClick={() => selectTimeSlot(slot)}
                            >
                                <ListItemText
                                    disableTypography
                                    primary={
                                        <Typography variant='h5'>{slot.localisedTime}</Typography>
                                    }
                                    secondary={
                                        <AvailableSlotsWorkersCount
                                            isIn={alreadyIn(slot.id)}
                                            workers={slot.availableWorkers}
                                            lastSlot={slot.lastSlot}
                                        />
                                    }
                                />
                                <AvailableSlotsPriceIcon price={slot.price} />
                            </ListItem>
                            <Divider component='li' />
                        </React.Fragment>
                    ))}
                </>
            )}
        </>
    );
};

export default AvailableSlotsListItem;
