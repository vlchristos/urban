import React from 'react';
import {
    Divider,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Typography,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

const AvailableSlotsListSkeleton = () => {
    return (
        <>
            {[0, 1, 2, 3].map((item) => (
                <React.Fragment key={item}>
                    <ListItem>
                        <ListItemText
                            disableTypography
                            primary={
                                <Typography variant='h5'>
                                    <Skeleton variant='text' width={150} />
                                </Typography>
                            }
                            secondary={<Skeleton variant='text' width={100} />}
                        />
                        <ListItemSecondaryAction>
                            <Skeleton variant='text' width={50} />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider component='li' />
                </React.Fragment>
            ))}
        </>
    );
};

export default AvailableSlotsListSkeleton;
