import React from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles, ListItemSecondaryAction, Box, Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    secondaryAction: {
        pointerEvents: 'none',
    },
}));
const AvailableSlotsPriceIcon = ({ price }) => {
    const classes = useStyles();
    return (
        <ListItemSecondaryAction className={classes.secondaryAction}>
            <Box display='flex'>
                <Box mr={1}>
                    <Typography component='span' variant='subtitle1'>
                        {price}
                    </Typography>
                </Box>
                <ArrowForwardIosIcon />
            </Box>
        </ListItemSecondaryAction>
    );
};

export default AvailableSlotsPriceIcon;
