import React from 'react';
import { Typography } from '@material-ui/core';

const AvailableSlotsWorkersCount = ({ workers, lastSlot, isIn }) => {
    return (
        <>
            {isIn ? (
                <Typography variant='subtitle2' color='primary'>
                    Already in your basket
                </Typography>
            ) : (
                <Typography color={lastSlot ? 'error' : 'textSecondary'} variant='subtitle2'>
                    {lastSlot && 'Only '}
                    {workers} available
                </Typography>
            )}
        </>
    );
};

export default AvailableSlotsWorkersCount;
