import { Badge, IconButton } from '@material-ui/core';
import React from 'react';
import { useSelector } from 'react-redux';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import { Link } from 'react-router-dom';
const ListBasketIcon = () => {
    const showBasket = useSelector((state) => state.global.hasItemsInBasket);
    const basketItems = useSelector((state) => state.global.basketItems);
    return (
        <IconButton component={Link} to='/basket' disabled={!showBasket} aria-label='cart'>
            <Badge
                badgeContent={basketItems.length}
                color='secondary'
                overlap='circular'
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
            >
                <ShoppingBasketIcon color={showBasket ? 'primary' : 'disabled'} />
            </Badge>
        </IconButton>
    );
};

export default ListBasketIcon;
