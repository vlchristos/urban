import React from 'react';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import {
    Box,
    DialogActions,
    DialogContent,
    FormControlLabel,
    Switch,
    List,
    ListItem,
    ListItemText,
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { setCorsFix } from '../store/global/globalActions';
import { makeStyles } from '@material-ui/core/styles';

function CorsDialog(props) {
    const dispatch = useDispatch();
    const cors = useSelector((state) => state.global.corsFix);
    const { onClose, open } = props;

    const handleClose = () => {
        onClose();
    };

    const handleSwitch = () => {
        dispatch(setCorsFix());
    };

    return (
        <Dialog onClose={handleClose} aria-labelledby='cors-fix-dialog' open={open}>
            <DialogTitle id='cors-fix-dialog'>Fixing CORS</DialogTitle>
            <DialogContent dividers>
                <Typography gutterBottom>
                    If you don't see any data is probably due to Cross-Origin issues! Try to fix the
                    Cors problem with 'cors-anywhere' service.
                </Typography>
                <List>
                    <ListItem>
                        <Typography gutterBottom>
                            First, go to{' '}
                            <a
                                href='https://cors-anywhere.herokuapp.com/'
                                target='_blank'
                                rel='noreferrer'
                            >
                                https://cors-anywhere.herokuapp.com
                            </a>{' '}
                            and enable the service.
                        </Typography>
                    </ListItem>
                    <ListItem>
                        <ListItemText>
                            <Typography gutterBottom>Then turn On this switch</Typography>
                        </ListItemText>
                        <FormControlLabel
                            control={
                                <Switch
                                    checked={cors}
                                    onChange={handleSwitch}
                                    color='primary'
                                    name='cors'
                                    inputProps={{ 'aria-label': 'cors enable' }}
                                    label='Fix CORS'
                                />
                            }
                            label='Fix CORS Client side'
                        />
                    </ListItem>
                    <ListItem>
                        <Typography gutterBottom>Finally, reload the page</Typography>
                    </ListItem>
                </List>
                <Typography gutterBottom color='textSecondary'>
                    If this won't work, then you can either download an extension like{' '}
                    <a
                        target='_blank'
                        rel='noreferrer'
                        href='https://mybrowseraddon.com/access-control-allow-origin.html'
                    >
                        this
                    </a>{' '}
                    or fix it on API side
                </Typography>
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handleClose} color='primary'>
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    );
}
const useStyles = makeStyles((theme) => ({
    corsBtn: {
        position: 'absolute',
        top: theme.spacing(1),
        right: theme.spacing(1),
        [theme.breakpoints.up('md')]: {
            top: 'auto',
            right: 'auto',
            bottom: theme.spacing(1),
            left: theme.spacing(1),
        },
    },
}));

export default function CorsButton() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Box className={classes.corsBtn}>
            <Button onClick={handleClickOpen} size='small'>
                Cors
            </Button>
            <CorsDialog open={open} onClose={handleClose} />
        </Box>
    );
}
