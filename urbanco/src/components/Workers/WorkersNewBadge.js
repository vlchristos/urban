import { Chip, ListItemSecondaryAction, Typography, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(() => ({
    secondaryAction: {
        pointerEvents: 'none',
    },
}));
const WorkersNewBadge = () => {
    const classes = useStyles();
    return (
        <ListItemSecondaryAction className={classes.secondaryAction}>
            <Chip
                size='small'
                label={
                    <Typography variant='caption' color='textSecondary'>
                        New
                    </Typography>
                }
                color='primary'
            />
        </ListItemSecondaryAction>
    );
};

export default WorkersNewBadge;
