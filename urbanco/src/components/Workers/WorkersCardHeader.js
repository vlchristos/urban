import React from 'react';
import { CardHeader, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import ListBasketIcon from '../ListBasketIcon';

const WorkersCardHeader = () => {
    const selectedSlot = useSelector((state) => state.global.selectedSlot);

    return (
        <CardHeader
            avatar={<EventAvailableIcon color='primary' fontSize='large' />}
            title={
                <Typography variant='h5'>
                    {selectedSlot.lastSlot
                        ? 'Only 1 available session'
                        : `${selectedSlot.availableWorkers} available sessions`}
                </Typography>
            }
            subheader={`Price: ${selectedSlot.price} | Time: ${selectedSlot.localisedTime}`}
            action={<ListBasketIcon />}
        />
    );
};

export default WorkersCardHeader;
