import React from 'react';
import { Box, Typography } from '@material-ui/core';

const WorkersListItemName = ({ name, isNew }) => {
    return (
        <Box display='flex'>
            <Box mr={1}>
                <Typography variant='h6'>{name}</Typography>
            </Box>
            {isNew && (
                <Typography variant='caption' color='secondary'>
                    New
                </Typography>
            )}
        </Box>
    );
};

export default WorkersListItemName;
