import { Button, CardActions, Box } from '@material-ui/core';
import React from 'react';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { Link } from 'react-router-dom';

const WorkersBackToSlots = () => {
    return (
        <CardActions>
            <Box px={2}>
                <Button startIcon={<ArrowBackIosIcon />} component={Link} to='/' size='small'>
                    Time slots
                </Button>
            </Box>
        </CardActions>
    );
};

export default WorkersBackToSlots;
