import React from 'react';
import {
    ListItem,
    ListItemText,
    Divider,
    ListItemAvatar,
    Avatar,
    Typography,
    makeStyles,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
const useStyles = makeStyles(() => ({
    avatarWrapper: {
        minWidth: 68,
    },
    avatar: {
        width: 52,
        height: 52,
    },
}));
const WorkersListSkeleton = () => {
    const classes = useStyles();
    return (
        <>
            {[0, 1, 2, 3].map((item) => (
                <React.Fragment key={item}>
                    <ListItem>
                        <ListItemAvatar className={classes.avatarWrapper}>
                            <Avatar className={classes.avatar}>
                                <Skeleton variant='circle' width={52} height={52} />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            disableTypography
                            primary={
                                <Typography variant='h6'>
                                    <Skeleton variant='text' width={150} />
                                </Typography>
                            }
                            secondary={
                                <Typography variant='caption' color='secondary'>
                                    <Skeleton variant='text' width={100} />
                                </Typography>
                            }
                        />
                    </ListItem>
                    <Divider component='li' />
                </React.Fragment>
            ))}
        </>
    );
};

export default WorkersListSkeleton;
