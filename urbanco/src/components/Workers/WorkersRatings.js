import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { Rating } from '@material-ui/lab';

const WorkersRatings = ({ rating }) => {
    return (
        <Box display='flex'>
            <Typography color='textSecondary' aria-label='Rating' variant='caption'>
                {rating}
            </Typography>
            <Rating value={rating} size='small' name='worker rating' readOnly precision={0.1} />
        </Box>
    );
};

export default WorkersRatings;
