import React from 'react';
import { ListItemSecondaryAction, makeStyles } from '@material-ui/core';
import DoneIcon from '@material-ui/icons/Done';

const useStyles = makeStyles(() => ({
    secondaryAction: {
        pointerEvents: 'none',
    },
}));

const WorkersListItemAddedToCart = () => {
    const classes = useStyles();
    return (
        <ListItemSecondaryAction className={classes.secondaryAction}>
            <DoneIcon color='primary' fontSize='large' />
        </ListItemSecondaryAction>
    );
};

export default WorkersListItemAddedToCart;
