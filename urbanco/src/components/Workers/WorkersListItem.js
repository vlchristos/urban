import React from 'react';
import {
    Avatar,
    Divider,
    ListItem,
    ListItemAvatar,
    ListItemText,
    makeStyles,
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import WorkersRatings from './WorkersRatings';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedWorker } from '../../store/global/globalActions';
import WorkersListItemAddedToCart from './WorkersListItemAddedToCart';
import WorkersListItemName from './WorkersListItemName';
import { isEmpty } from 'lodash';
import WorkersListSkeleton from './WorkersListSkeleton';

const useStyles = makeStyles(() => ({
    avatarWrapper: {
        minWidth: 68,
    },
    avatar: {
        width: 52,
        height: 52,
    },
}));

const WorkersListItem = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const workers = useSelector((state) => state.availableWorkers.workers);
    const selectedSlot = useSelector((state) => state.global.selectedSlot);
    const basketItems = useSelector((state) => state.global.basketItems);
    const [isSelected, setIsSelected] = React.useState(null);

    function selectWorker(worker) {
        setIsSelected(worker.id);
        dispatch(setSelectedWorker(worker));
    }

    React.useEffect(() => {
        basketItems.forEach((item) => {
            if (item.slot_id === selectedSlot.id) {
                setIsSelected(item.worker_id);
            }
        });
        return () => {
            setIsSelected(null);
        };
    }, [basketItems, selectedSlot, selectedSlot.id]);

    return (
        <>
            {isEmpty(workers) ? (
                <WorkersListSkeleton />
            ) : (
                <>
                    {workers.map((worker) => (
                        <React.Fragment key={worker.id}>
                            <ListItem
                                button
                                selected={isSelected === worker.id}
                                onClick={() => selectWorker(worker)}
                            >
                                <ListItemAvatar className={classes.avatarWrapper}>
                                    <Avatar className={classes.avatar}>
                                        <PersonIcon />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    disableTypography
                                    primary={
                                        <WorkersListItemName
                                            name={worker.name}
                                            isNew={worker.isNew}
                                        />
                                    }
                                    secondary={<WorkersRatings rating={Number(worker.rating)} />}
                                />
                                {isSelected === worker.id && <WorkersListItemAddedToCart />}
                            </ListItem>
                            <Divider component='li' />
                        </React.Fragment>
                    ))}
                </>
            )}
        </>
    );
};

export default WorkersListItem;
