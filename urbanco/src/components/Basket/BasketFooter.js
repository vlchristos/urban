import React from 'react';
import { Box, Button } from '@material-ui/core';
import PaymentIcon from '@material-ui/icons/Payment';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { isEmpty } from 'lodash';
import { clearBasket } from '../../store/global/globalActions';
import { useHistory } from 'react-router';

const BasketFooter = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const basketList = useSelector((state) => state.basket.basketList);
    function bookNow() {
        dispatch(clearBasket());
        history.push('/');
    }
    return (
        <Box display='flex' justifyContent='space-between' py={2}>
            <Button
                component={Link}
                to='/workers'
                startIcon={<ArrowBackIosIcon />}
                size='large'
                variant='outlined'
            >
                Back
            </Button>
            {!isEmpty(basketList) && (
                <Button
                    endIcon={<PaymentIcon />}
                    size='large'
                    variant='contained'
                    color='primary'
                    onClick={() => bookNow()}
                >
                    Book Now
                </Button>
            )}
        </Box>
    );
};

export default BasketFooter;
