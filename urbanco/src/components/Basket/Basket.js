import React from 'react';
import { Box, Card, CardContent, CardHeader, Container, Divider } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { loadBasket } from '../../store/basket/basketActions';
import BasketFooter from './BasketFooter';
import BasketList from './BasketList';
import BasketTotal from './BasketTotal';

const Basket = () => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(loadBasket());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <Box py={4}>
            <Container maxWidth='md'>
                <Card>
                    <CardHeader title='Booking details' />
                    <Divider />
                    <CardContent>
                        <BasketList />
                        <BasketTotal />
                    </CardContent>
                </Card>
                <BasketFooter />
            </Container>
        </Box>
    );
};

export default Basket;
