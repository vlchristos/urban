import React from 'react';
import { List } from '@material-ui/core';
import { useSelector } from 'react-redux';
import BasketListItem from './BasketListItem';
import { isEmpty } from 'lodash';
import BasketEmptyState from './BasketEmptyState';

const BasketList = () => {
    const basketList = useSelector((state) => state.basket.basketList);
    return (
        <>
            {isEmpty(basketList) ? (
                <BasketEmptyState />
            ) : (
                <List>
                    <>
                        {basketList.map((listItem) => (
                            <BasketListItem
                                key={listItem.slot_id}
                                time={listItem.time}
                                name={listItem.name}
                                rating={listItem.rating}
                                price={listItem.price}
                                id={listItem.slot_id}
                            />
                        ))}
                    </>
                </List>
            )}
        </>
    );
};

export default BasketList;
