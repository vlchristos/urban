import React from 'react';
import {
    Avatar,
    Hidden,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Typography,
    ListItemSecondaryAction,
    IconButton,
    Divider,
} from '@material-ui/core';
import WorkersRatings from '../Workers/WorkersRatings';
import DeleteIcon from '@material-ui/icons/Delete';
import PersonIcon from '@material-ui/icons/Person';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import { useDispatch } from 'react-redux';
import { removeBasketItem } from '../../store/global/globalActions';

const BasketListItem = ({ id, time, name, rating, price }) => {
    const dispatch = useDispatch();
    function deleteItem() {
        dispatch(removeBasketItem(id));
    }
    return (
        <>
            <ListItem>
                <Hidden xsDown>
                    <ListItemAvatar>
                        <Avatar>
                            <EventAvailableIcon />
                        </Avatar>
                    </ListItemAvatar>
                </Hidden>
                <ListItemText primary={<Typography variant='h5'>{time}</Typography>} />
                <Hidden xsDown>
                    <ListItemAvatar>
                        <Avatar>
                            <PersonIcon />
                        </Avatar>
                    </ListItemAvatar>
                </Hidden>
                <ListItemText
                    disableTypography
                    primary={name}
                    secondary={<WorkersRatings rating={Number(rating)} />}
                />
                <ListItemText primary={price} />
                <ListItemSecondaryAction>
                    <IconButton onClick={() => deleteItem(id)}>
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
            <Divider component='li' />
        </>
    );
};

export default BasketListItem;
