import React from 'react';
import { Box, Button, Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';

const BasketEmptyState = () => {
    return (
        <Box textAlign='center'>
            <Typography color='textSecondary' variant='h4'>
                Your basket is empty
            </Typography>
            <Typography color='textSecondary' variant='h4'>
                :(
            </Typography>
            <Box mt={3}>
                <Button variant='outlined' size='large' component={Link} to='/'>
                    Home
                </Button>
            </Box>
        </Box>
    );
};

export default BasketEmptyState;
