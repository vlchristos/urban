import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { isEmpty } from 'lodash';

const BasketTotal = () => {
    const sum = useSelector((state) => state.basket.basketTotalPrice);
    const basketList = useSelector((state) => state.basket.basketList);
    return (
        <>
            {!isEmpty(basketList) && (
                <Box align='right' pt={2}>
                    <Typography align='right' variant='body1'>
                        Total: &#163;{sum}
                    </Typography>
                </Box>
            )}
        </>
    );
};

export default BasketTotal;
