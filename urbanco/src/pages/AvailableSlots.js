import React from 'react';
import {
    Box,
    Card,
    CardContent,
    CardHeader,
    Divider,
    List,
    makeStyles,
    Typography,
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import AvailableSlotsListItem from '../components/Slots/AvailableSlotsListItem';
import { loadAvailableSlots } from '../store/availableSlots/availableSlotsActions';
import { useDispatch } from 'react-redux';
import ListBasketIcon from '../components/ListBasketIcon';

const useStyles = makeStyles((theme) => ({
    card: {
        height: '100%',
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '95%',
        },
        marginLeft: 'auto',
    },
    cardContent: {
        height: '100%',
        overflowY: 'auto',
        overflowX: 'hidden',
    },
}));

const AvailableSlots = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(loadAvailableSlots());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <Box
            component={Card}
            elevation={0}
            display='flex'
            flexDirection='column'
            className={classes.card}
        >
            <CardHeader
                avatar={<AccessTimeIcon color='primary' fontSize='large' />}
                title={<Typography variant='h5'>Availability</Typography>}
                subheader='What time suits you?'
                action={<ListBasketIcon />}
            />
            <Divider />
            <CardContent className={classes.cardContent}>
                <List component='nav' aria-label='available time slots'>
                    <AvailableSlotsListItem />
                </List>
            </CardContent>
        </Box>
    );
};

export default AvailableSlots;
