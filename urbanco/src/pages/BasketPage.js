import React from 'react';
import { Container, Typography, Box, makeStyles } from '@material-ui/core';
import bg from '../img/basket-bg.jpg';
import Basket from '../components/Basket/Basket';

const useStyles = makeStyles((theme) => ({
    header: {
        backgroundColor: '#EBEBEB',
        backgroundImage: `url(${bg})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        height: 'auto',
        [theme.breakpoints.up('sm')]: {
            height: 250,
        },
    },
    headerInner: {
        height: '100%',
        backgroundColor: 'rgba(255,255,255,0.3)',
        backdropFilter: 'blur(4px)',
    },
    container: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
    },
}));
const BasketPage = () => {
    const classes = useStyles();
    return (
        <main>
            <header className={classes.header}>
                <div className={classes.headerInner}>
                    <Box component={Container} py={4} maxWidth='md' className={classes.container}>
                        <Typography align='center' variant='h2' component='h1'>
                            Basket
                        </Typography>
                    </Box>
                </div>
            </header>
            <Basket />
        </main>
    );
};

export default BasketPage;
