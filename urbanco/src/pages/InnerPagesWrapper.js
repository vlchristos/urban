import React from 'react';
import { Switch, Route } from 'react-router-dom';
import AvailableSlots from './AvailableSlots';
import AvailableWorkers from './AvailableWorkers';

const InnerPagesWrapper = () => {
    return (
        <Switch>
            <Route path='/workers'>
                <AvailableWorkers />
            </Route>
            <Route path='/'>
                <AvailableSlots />
            </Route>
        </Switch>
    );
};

export default InnerPagesWrapper;
