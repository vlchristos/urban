import React from 'react';
import { Box, Card, CardContent, Divider, List, makeStyles } from '@material-ui/core';
import WorkersListItem from '../components/Workers/WorkersListItem';
import WorkersBackToSlots from '../components/Workers/WorkersBackToSlots';
import { useDispatch } from 'react-redux';
import { clearWorkesList, loadWorkers } from '../store/workers/workersActions';
import { clearSelectedWorker } from '../store/global/globalActions';
import WorkersCardHeader from '../components/Workers/WorkersCardHeader';
const useStyles = makeStyles((theme) => ({
    card: {
        height: '100%',
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '95%',
        },
        marginLeft: 'auto',
    },
    cardContent: {
        height: '100%',
        overflowY: 'auto',
        overflowX: 'hidden',
    },
}));

const AvailableWorkers = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(loadWorkers());
        return () => {
            dispatch(clearWorkesList());
            dispatch(clearSelectedWorker());
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Box
            component={Card}
            elevation={0}
            display='flex'
            flexDirection='column'
            className={classes.card}
        >
            <WorkersCardHeader />
            <Divider />
            <WorkersBackToSlots />
            <Divider />
            <CardContent className={classes.cardContent}>
                <List component='nav' aria-label='available sessions'>
                    <WorkersListItem />
                </List>
            </CardContent>
        </Box>
    );
};

export default AvailableWorkers;
