import React from 'react';
import BasketPage from './BasketPage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import PageStructure from './PageStructure';

const PagesWrapper = () => {
    return (
        <Router>
            <Switch>
                <Route path='/basket'>
                    <BasketPage />
                </Route>
                <Route path='/'>
                    <PageStructure />
                </Route>
            </Switch>
        </Router>
    );
};

export default PagesWrapper;
