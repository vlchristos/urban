import { makeStyles, Typography, Box } from '@material-ui/core';
import React from 'react';
import CorsButton from '../components/CorsButton';
import bg from '../img/index-bg.jpg';
import InnerPagesWrapper from './InnerPagesWrapper';

const useStyles = makeStyles((theme) => ({
    main: {
        height: '100vh',
        backgroundColor: '#EBEBEB',
        backgroundImage: `url(${bg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain',
        backgroundPosition: 'top left',
        overflow: 'hidden',
        [theme.breakpoints.up('md')]: {
            backgroundPosition: 'bottom left',
        },
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row',
        },
    },
    tagLine: {
        margin: 'auto',
        [theme.breakpoints.up('md')]: {
            margin: '15% auto',
        },
    },
    tagLineWrapper: {
        display: 'flex',
        height: '30%',
        backdropFilter: 'blur(5px)',
        backgroundColor: 'rgba(255,255,255,0.5)',
        [theme.breakpoints.up('md')]: {
            height: '100%',
            width: '60%',
        },
    },
    contentWrapper: {
        display: 'flex',
        height: '70%',
        padding: theme.spacing(1),
        background: theme.palette.background.paper,
        [theme.breakpoints.up('md')]: {
            background: 'rgba(255,255,255,0.7)',
            height: '100%',
            width: '40%',
        },
    },
}));

const PageStructure = () => {
    const classes = useStyles();
    return (
        <main className={classes.main}>
            <Box className={classes.content}>
                <Box className={classes.tagLineWrapper}>
                    <CorsButton />
                    <Box className={classes.tagLine}>
                        <Typography compoment='h1' variant='h4'>
                            Your next session
                        </Typography>
                        <Typography compoment='h2' variant='h1' color='textSecondary'>
                            Starts here
                        </Typography>
                    </Box>
                </Box>
                <Box className={classes.contentWrapper}>
                    <InnerPagesWrapper />
                </Box>
            </Box>
        </main>
    );
};

export default PageStructure;
