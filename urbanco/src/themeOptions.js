import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';

export const themeOptions = () => {
    let theme = createTheme({
        palette: {
            primary: {
                main: '#7BA849',
            },
            secondary: {
                main: '#dd4242',
            },
        },
    });
    return (theme = responsiveFontSizes(theme));
};
