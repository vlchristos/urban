import { store } from '../store/store';
import { isLoading } from '../store/global/globalActions';
import axios from 'axios';

const apiPaths = {
    corsFix: 'https://cors-anywhere.herokuapp.com/',
    timeSlots: 'https://storage.googleapis.com/urban-technical/slots.json',
    workers: 'https://storage.googleapis.com/urban-technical/workers.json',
    availableWorkers: 'https://storage.googleapis.com/urban-technical/available-workers.json',
};

// Axios GET
const fetch = (url) => {
    // We can use this for loading states
    store.dispatch(isLoading(true));

    // Hack for CORS problem
    const {
        global: { corsFix },
    } = store.getState();
    const callingUrl = corsFix ? apiPaths.corsFix + url : url;
    return axios
        .get(callingUrl)
        .then((response) => {
            // And stop the loading state
            store.dispatch(isLoading(false));
            return { response };
        })
        .catch((error) => {
            store.dispatch(isLoading(false));
            return { error };
        });
};

export const Api = {
    getTimeSlots: () => fetch(apiPaths.timeSlots),
    getWorkers: () => fetch(apiPaths.workers),
    getAvailableWorkers: () => fetch(apiPaths.availableWorkers),
};
