import { all, spawn, call } from 'redux-saga/effects';
import globalSagas from './global/globalSagas';
import availableSlotsSagas from './availableSlots/availableSlotsSagas';
import workersSagas from './workers/workersSagas';
import basketSagas from './basket/basketSagas';

export default function* rootSaga() {
    const sagas = [globalSagas, availableSlotsSagas, workersSagas, basketSagas];

    yield all(
        sagas.map((saga) =>
            spawn(function* () {
                while (true) {
                    try {
                        yield call(saga);
                        break;
                    } catch (e) {
                        console.log(e);
                    }
                }
            }),
        ),
    );
}
