import { takeEvery, call, all, put } from 'redux-saga/effects';
import { AVAILABLE_SLOTS_LIST_SET, AVAILABLE_SLOTS_LOAD } from './availableSlotsActionTypes';
import { Api } from '../../api/api';
import { slotsList } from './models';

function* availableSlotsSagas() {
    // Fetch both time slots & available workers
    function* loadSlotsPage() {
        const [slots, availableWorkers] = yield all([
            call(fetchSlots),
            call(fetchAvailableWorkers),
        ]);
        if (slots && availableWorkers) {
            // Combine slots with available wokers
            const list = yield call(slotsList, slots, availableWorkers);
            yield call(setSlotsList, list);
        }
    }

    // Fetch time slots
    function* fetchSlots() {
        const { response, error } = yield call(Api.getTimeSlots);
        if (response) {
            let slots = response.data.slots;
            return slots;
        } else {
            console.log('error :>> ', error);
        }
    }

    // Fetch available workers
    function* fetchAvailableWorkers() {
        const { response, error } = yield call(Api.getAvailableWorkers);
        if (response) {
            let availableWorkers = response.data['available-workers'];
            return availableWorkers;
        } else {
            console.log('error :>> ', error);
        }
    }

    // Persist slots list to store
    function* setSlotsList(data) {
        yield put({ type: AVAILABLE_SLOTS_LIST_SET, payload: data });
    }

    // On every page laod
    yield takeEvery(AVAILABLE_SLOTS_LOAD, loadSlotsPage);
}
export default availableSlotsSagas;
