import { AVAILABLE_SLOTS_LIST_SET } from './availableSlotsActionTypes';

const initialState = () => ({
    slotsList: [],
});

export const availableSlotsReducer = (state = initialState(), { type, payload }) => {
    switch (type) {
        case AVAILABLE_SLOTS_LIST_SET:
            return { ...state, slotsList: payload };
        default:
            return state;
    }
};
