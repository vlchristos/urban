import { AVAILABLE_SLOTS_LOAD } from './availableSlotsActionTypes';

export const loadAvailableSlots = () => ({
    type: AVAILABLE_SLOTS_LOAD,
});
