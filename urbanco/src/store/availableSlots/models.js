// Massage data and return the ready list to render
export const slotsList = (slots, availableWorkers) => {
    // Find the slot & count available workers
    function availableWorkerIds(slot_id) {
        return availableWorkers.find((slot) => slot.slot_id === slot_id).availableWorker_ids;
    }

    // Combine slots with available workers
    const list = slots.map((slot) => {
        let workers = availableWorkerIds(slot.id);
        let workersLenght = workers.length;
        return {
            ...slot,
            availableWorker_ids: workers,
            availableWorkers: workersLenght,
            lastSlot: workersLenght === 1,
            unavailable: workersLenght === 0,
        };
    });

    return list;
};
