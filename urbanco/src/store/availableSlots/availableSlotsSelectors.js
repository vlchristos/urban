import { store } from '../store';

export const getAvailableSlotsState = () => {
    const {
        availableSlots: { slotsList },
    } = store.getState();

    return {
        slotsList,
    };
};
