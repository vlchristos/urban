import {
    SELECTED_WORKER_CLEAR,
    IS_LOADING,
    SELECTED_SLOT_SET,
    SELECTED_WORKER_SET,
    BASKET_ITEM_REMOVE,
    BASKET_CLEAR,
    CORS_FIX_SET,
} from './globalActionTypes';

export const isLoading = (state) => ({
    type: IS_LOADING,
    payload: state,
});

export const setSelectedSlot = (slot) => ({
    type: SELECTED_SLOT_SET,
    payload: slot,
});

export const setSelectedWorker = (worker) => ({
    type: SELECTED_WORKER_SET,
    payload: worker,
});

export const clearSelectedWorker = () => ({
    type: SELECTED_WORKER_CLEAR,
});
export const removeBasketItem = (slot) => ({
    type: BASKET_ITEM_REMOVE,
    payload: slot,
});
export const clearBasket = () => ({
    type: BASKET_CLEAR,
});
export const setCorsFix = () => ({
    type: CORS_FIX_SET,
});
