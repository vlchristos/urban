import {
    BASKET_ITEM_SET,
    HAS_ITEMS_IN_BASKET,
    SELECTED_WORKER_SET,
    BASKET_ITEM_REMOVE,
    BASKET_CLEAR,
} from './globalActionTypes';
import { takeEvery, select, put, call } from 'redux-saga/effects';
import { getGlobalState } from './globalSelectors';
import { basketItem } from './model';

function* globalSagas() {
    function* setBasketItems() {
        const { selectedWorker, selectedSlot, basketItems } = yield select(getGlobalState);
        const items = yield call(basketItem, selectedWorker, selectedSlot, basketItems);
        yield put({ type: BASKET_ITEM_SET, payload: items });
    }

    function* setHasBasket() {
        const { basketItems } = yield select(getGlobalState);
        let hasItems = basketItems.length !== 0;
        yield put({ type: HAS_ITEMS_IN_BASKET, payload: hasItems });
    }

    function* removeBasketItem({ payload }) {
        const { basketItems } = yield select(getGlobalState);
        const newBasket = basketItems.filter((item) => item.slot_id !== payload);
        yield put({ type: BASKET_ITEM_SET, payload: newBasket });
    }

    yield takeEvery(SELECTED_WORKER_SET, setBasketItems);
    yield takeEvery([BASKET_ITEM_SET, BASKET_CLEAR], setHasBasket);
    yield takeEvery(BASKET_ITEM_REMOVE, removeBasketItem);
}
export default globalSagas;
