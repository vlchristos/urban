import { store } from '../store';

export const getGlobalState = () => {
    const {
        global: { selectedSlot, selectedWorker, basketItems },
    } = store.getState();

    return {
        selectedSlot,
        selectedWorker,
        basketItems,
    };
};
