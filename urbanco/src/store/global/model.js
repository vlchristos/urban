export const basketItem = (worker, slot, basket) => {
    const newItem = {
        slot_id: slot.id,
        worker_id: worker.id,
    };

    const foundItem = basket.find((item) => item.slot_id === slot.id);

    if (foundItem) {
        foundItem.worker_id = worker.id;
        return basket;
    } else {
        return [...basket, newItem];
    }
};
