import {
    SELECTED_WORKER_CLEAR,
    SELECTED_SLOT_SET,
    IS_LOADING,
    SELECTED_WORKER_SET,
    HAS_ITEMS_IN_BASKET,
    BASKET_ITEM_SET,
    BASKET_CLEAR,
    CORS_FIX_SET,
} from './globalActionTypes';

const initialState = () => ({
    isLoading: false,
    selectedSlot: {
        localisedTime: '',
        price: '',
        availableWorkers: 0,
        lastSlot: false,
        unavailable: false,
    },
    selectedWorker: {},
    basketItems: [],
    hasItemsInBasket: false,
    corsFix: false,
});

export const globalReducer = (state = initialState(), { type, payload }) => {
    switch (type) {
        case IS_LOADING:
            return { ...state, isLoading: payload };
        case SELECTED_SLOT_SET:
            return { ...state, selectedSlot: payload };
        case SELECTED_WORKER_SET:
            return { ...state, selectedWorker: payload };
        case SELECTED_WORKER_CLEAR:
            return { ...state, selectedWorker: {} };
        case HAS_ITEMS_IN_BASKET:
            return { ...state, hasItemsInBasket: payload };
        case BASKET_ITEM_SET:
            return { ...state, basketItems: payload };
        case BASKET_CLEAR:
            return { ...state, basketItems: [] };
        case CORS_FIX_SET:
            return { ...state, corsFix: !state.corsFix };
        default:
            return state;
    }
};
