import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import localforage from 'localforage';
import { globalReducer } from './global/globalReducer';
import { availableSlotsReducer } from './availableSlots/availableSlotsReducer';
import { workersReducer } from './workers/workersReducer';
import { basketReducer } from './basket/basketReducer';

const storage = localforage.createInstance({
    name: 'App State',
});

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['global', 'availableSlots', 'availableWorkers', 'basket'],
};

const rootReducer = combineReducers({
    global: globalReducer,
    availableSlots: availableSlotsReducer,
    availableWorkers: workersReducer,
    basket: basketReducer,
});

export const persistedReducer = persistReducer(persistConfig, rootReducer);
