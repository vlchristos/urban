import { store } from '../store';

export const getWorkersState = () => {
    const {
        availableWorkers: { workersPool },
    } = store.getState();

    return {
        workersPool,
    };
};
