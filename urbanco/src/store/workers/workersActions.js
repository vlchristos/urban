import { WORKERS_LOAD, WORKERS_CLEAR } from './workersActionTypes';

export const loadWorkers = () => ({
    type: WORKERS_LOAD,
});
export const clearWorkesList = () => ({
    type: WORKERS_CLEAR,
});
