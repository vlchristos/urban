import { WORKERS_SET, WORKERS_CLEAR, WORKERS_POOL_SET } from './workersActionTypes';

const initialState = () => ({
    workers: [],
    workersPool: [],
});

export const workersReducer = (state = initialState(), { type, payload }) => {
    switch (type) {
        case WORKERS_POOL_SET:
            return { ...state, workersPool: payload };
        case WORKERS_SET:
            return { ...state, workers: payload };
        case WORKERS_CLEAR:
            return { ...state, workers: [] };
        default:
            return state;
    }
};
