export const findAvailableWorkes = (workers, availableWorker_ids) => {
    if (availableWorker_ids && workers) {
        return availableWorker_ids.map((worker_id) =>
            workers.find((worker) => worker.id === worker_id),
        );
    } else {
        return [];
    }
};
