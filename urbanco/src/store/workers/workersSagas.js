import { WORKERS_LOAD, WORKERS_POOL_SET, WORKERS_SET } from './workersActionTypes';
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { Api } from '../../api/api';
import { getGlobalState } from '../global/globalSelectors';
import { findAvailableWorkes } from './models';
import { getWorkersState } from './workersSelectors';
import { isEmpty } from 'lodash';

function* workersSagas() {
    // Load workers list
    function* loadWorkers() {
        const { response, error } = yield call(Api.getWorkers);
        if (response) {
            const { selectedSlot } = yield select(getGlobalState);
            const availableWorkers = findAvailableWorkes(
                response.data.workers,
                selectedSlot.availableWorker_ids,
            );
            yield call(setWorkers, availableWorkers);
            yield call(setWorkersPool, response.data.workers);
        } else {
            console.log('error :>> ', error);
        }
    }

    // Set available workers
    function* setWorkers(data) {
        yield put({ type: WORKERS_SET, payload: data });
    }
    // Fill the workers pool only on when empty
    function* setWorkersPool(data) {
        const { workersPool } = yield select(getWorkersState);
        if (isEmpty(workersPool)) {
            yield put({ type: WORKERS_POOL_SET, payload: data });
        }
    }

    yield takeEvery(WORKERS_LOAD, loadWorkers);
}
export default workersSagas;
