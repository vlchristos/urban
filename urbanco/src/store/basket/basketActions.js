import { BASKET_LOAD } from './basketActionTypes';

export const loadBasket = () => ({
    type: BASKET_LOAD,
});
