import { takeEvery, select, put, call } from 'redux-saga/effects';
import { getGlobalState } from '../global/globalSelectors';
import { getAvailableSlotsState } from '../availableSlots/availableSlotsSelectors';
import { getWorkersState } from '../workers/workersSelectors';
import { BASKET_LIST_SET, BASKET_LOAD, BASKET_TOTAL_PRICE_SET } from './basketActionTypes';
import { basketListItems, calcTotalPrice } from './model';
import { getBasketState } from './basketSelectors';
import { BASKET_CLEAR, BASKET_ITEM_SET } from '../global/globalActionTypes';
import { isEmpty } from 'lodash';

function* basketSagas() {
    function* setBasket() {
        const { basketItems } = yield select(getGlobalState);
        const { slotsList } = yield select(getAvailableSlotsState);
        const { workersPool } = yield select(getWorkersState);

        const list = yield call(basketListItems, basketItems, slotsList, workersPool);
        yield put({ type: BASKET_LIST_SET, payload: list });
    }
    function* setBasketPrice() {
        const { basketList } = yield select(getBasketState);
        if (isEmpty(basketList)) {
            yield put({ type: BASKET_TOTAL_PRICE_SET, payload: 0 });
        } else {
            const sum = yield call(calcTotalPrice, basketList);
            yield put({ type: BASKET_TOTAL_PRICE_SET, payload: sum });
        }
    }
    yield takeEvery([BASKET_LOAD, BASKET_ITEM_SET, BASKET_CLEAR], setBasket);
    yield takeEvery([BASKET_LIST_SET, BASKET_ITEM_SET, BASKET_CLEAR], setBasketPrice);
}
export default basketSagas;
