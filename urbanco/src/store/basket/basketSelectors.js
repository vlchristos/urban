import { store } from '../store';

export const getBasketState = () => {
    const {
        basket: { basketList },
    } = store.getState();

    return {
        basketList,
    };
};
