import { BASKET_LIST_SET, BASKET_TOTAL_PRICE_SET } from './basketActionTypes';
const initialState = () => ({
    basketList: [],
    basketTotalPrice: 0,
});

export const basketReducer = (state = initialState(), { type, payload }) => {
    switch (type) {
        case BASKET_LIST_SET:
            return { ...state, basketList: payload };
        case BASKET_TOTAL_PRICE_SET:
            return { ...state, basketTotalPrice: payload };
        default:
            return state;
    }
};
