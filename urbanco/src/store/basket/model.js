export const basketListItems = (basketItems, slots, workers) => {
    const list = basketItems.map((item, i) => {
        const slot = slots.find((slot) => slot.id === item.slot_id);
        const worker = workers.find((worker) => worker.id === item.worker_id);

        const listItem = {
            slot_id: slot.id,
            time: slot.localisedTime,
            price: slot.price,
            name: worker.name,
            rating: worker.rating,
            isNew: worker.isNew,
        };
        return listItem;
    });
    return list;
};

export const calcTotalPrice = (basket) => {
    const prices = basket.map((item) => Number(item.price.substring(1)));
    const sum = prices.reduce((a, b) => a + b).toFixed(2);
    return sum;
};
