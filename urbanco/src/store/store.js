import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist';
import { persistedReducer } from './rootReducer';
import createSagaMiddleware from 'redux-saga';
import sagas from './rootSaga';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
    persistedReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware)),
);
// then run the saga
sagaMiddleware.run(sagas);

export const persistor = persistStore(store);
